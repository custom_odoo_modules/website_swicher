# -*- coding: utf-8 -*-
{
    'name': "Переключатель сайтов",

    'summary': """
        Переключатель сайтов""",

    'author': "Xorikita",
    'website': "https://erpsmart.ru",
    'sequence': -2,
    'category': 'Website',
    'version': '15.0.1.0.0',

    'depends': ['website'],

    'data': [
        'views/templates.xml',
    ],
    'licence': 'LGPL-3'
}
